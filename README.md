# Deployment Pipeline Project

This is a showcase of deployment pipeline using Gitlab, with basic PHP code and very basic PHPunit tests included, to demonstrate execution of tests in the pipeline.

Taken approach is called "zero downtime deployment" which I find the most effective way to deploy code with minimal disruption to the service itself (all the code is prepared behind the scenes, and once everything is ready to use, symlink is repointed to recently deployed code - so it doesn't even require restarting Apache or making any changes to vhost setup). 
Obviously, the final steps can easily be replaced with deployment to another VM and repointing the load balancer, or deploying to a new container - the goal of this piece of code is to demostrate the possibilities of this approach.
Additional advantage is ease of rollback - it only requires repointing the symlink back to the previous directory. 

How it works:
Pipeline has 2 steps defined: testing and deployment. If the tests fail, pipeline will stop there and deployment will not occur.
I am utilising available free gitlab runners, but for the testing purposes I had a local gitlab runner on top of docker being used (self-hosted runners are recommended for more resource-hungry tasks). 
In testing phase, tests from phpunit_mysql.xml are executed, on success the script will run the following steps:
- setup SSH in container
- prepare package to deploy
- SSH into remote server and prepare deployment directory
- rsync over SSH prepared package (excluding unnecessary/unsafe files and directories)
- SSH into remote again to finalize the deployment:
    - get composer and run it
    - update index.html with commit SHA and deployment timestamp
    - cleanup unnecessary files
    - repoint symlink to deployed directory

I am using non-standard hosting provider (home.pl) hence few lines here and there to support this environment. 

Deployed code is available under http://deploymentpipeline.sobieszek.net

Potential improvements and assumption made in this project:
- project is master branch only, no branching model has been applied (in real life this would be required) 
- no Merge Requests - as above, this would be requirement if more people would be working on the project, with a policy defined for MRs (permissions, required sign-offs, etc.)
- currently code is automatically deployed on commit to master (trigger is set for the pipeline to start when new commit is done into master branch) - again, this is only a showcase, in real life this would be a different policy, based on environments and branches available
- post-deployment cleanup of earlier deployed artifacts, with retention policy
