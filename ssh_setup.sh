#!/bin/bash
# install and configure SSH agent
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)
mkdir -p ~/.ssh
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\nHost $DEPLOY_TARGET\n\tPort 22222\n\n" > ~/.ssh/config
# install private key
ssh-add <(echo "$SSH_PRIVATE_KEY" | base64 -d)
# add known hosts entry
(echo "$SSH_KNOWN_HOSTS" | base64 -d) >> ~/.ssh/known_hosts
# set correct permissions
chmod -R 0600 ~/.ssh
